Rails.application.routes.draw do
  resources :register_coffes
  get 'home/index'
  root 'home#index'

  devise_for :employees, :controllers => { registrations: 'registrations' }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
