require "application_system_test_case"

class CoffesTest < ApplicationSystemTestCase
  setup do
    @coffe = coffes(:one)
  end

  test "visiting the index" do
    visit coffes_url
    assert_selector "h1", text: "Coffes"
  end

  test "creating a Coffe" do
    visit coffes_url
    click_on "New Coffe"

    fill_in "Name", with: @coffe.name
    fill_in "Special", with: @coffe.special
    click_on "Create Coffe"

    assert_text "Coffe was successfully created"
    click_on "Back"
  end

  test "updating a Coffe" do
    visit coffes_url
    click_on "Edit", match: :first

    fill_in "Name", with: @coffe.name
    fill_in "Special", with: @coffe.special
    click_on "Update Coffe"

    assert_text "Coffe was successfully updated"
    click_on "Back"
  end

  test "destroying a Coffe" do
    visit coffes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Coffe was successfully destroyed"
  end
end
