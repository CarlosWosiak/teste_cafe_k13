require "application_system_test_case"

class RegisterCoffesTest < ApplicationSystemTestCase
  setup do
    @register_coffe = register_coffes(:one)
  end

  test "visiting the index" do
    visit register_coffes_url
    assert_selector "h1", text: "Register Coffes"
  end

  test "creating a Register coffe" do
    visit register_coffes_url
    click_on "New Register Coffe"

    fill_in "Coffe", with: @register_coffe.coffe_id
    fill_in "Date", with: @register_coffe.date
    fill_in "Employee", with: @register_coffe.employee_id
    click_on "Create Register coffe"

    assert_text "Register coffe was successfully created"
    click_on "Back"
  end

  test "updating a Register coffe" do
    visit register_coffes_url
    click_on "Edit", match: :first

    fill_in "Coffe", with: @register_coffe.coffe_id
    fill_in "Date", with: @register_coffe.date
    fill_in "Employee", with: @register_coffe.employee_id
    click_on "Update Register coffe"

    assert_text "Register coffe was successfully updated"
    click_on "Back"
  end

  test "destroying a Register coffe" do
    visit register_coffes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Register coffe was successfully destroyed"
  end
end
