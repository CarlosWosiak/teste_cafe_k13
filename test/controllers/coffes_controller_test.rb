require 'test_helper'

class CoffesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @coffe = coffes(:one)
  end

  test "should get index" do
    get coffes_url
    assert_response :success
  end

  test "should get new" do
    get new_coffe_url
    assert_response :success
  end

  test "should create coffe" do
    assert_difference('Coffe.count') do
      post coffes_url, params: { coffe: { name: @coffe.name, special: @coffe.special } }
    end

    assert_redirected_to coffe_url(Coffe.last)
  end

  test "should show coffe" do
    get coffe_url(@coffe)
    assert_response :success
  end

  test "should get edit" do
    get edit_coffe_url(@coffe)
    assert_response :success
  end

  test "should update coffe" do
    patch coffe_url(@coffe), params: { coffe: { name: @coffe.name, special: @coffe.special } }
    assert_redirected_to coffe_url(@coffe)
  end

  test "should destroy coffe" do
    assert_difference('Coffe.count', -1) do
      delete coffe_url(@coffe)
    end

    assert_redirected_to coffes_url
  end
end
