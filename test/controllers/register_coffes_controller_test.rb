require 'test_helper'

class RegisterCoffesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @register_coffe = register_coffes(:one)
  end

  test "should get index" do
    get register_coffes_url
    assert_response :success
  end

  test "should get new" do
    get new_register_coffe_url
    assert_response :success
  end

  test "should create register_coffe" do
    assert_difference('RegisterCoffe.count') do
      post register_coffes_url, params: { register_coffe: { coffe_id: @register_coffe.coffe_id, date: @register_coffe.date, employee_id: @register_coffe.employee_id } }
    end

    assert_redirected_to register_coffe_url(RegisterCoffe.last)
  end

  test "should show register_coffe" do
    get register_coffe_url(@register_coffe)
    assert_response :success
  end

  test "should get edit" do
    get edit_register_coffe_url(@register_coffe)
    assert_response :success
  end

  test "should update register_coffe" do
    patch register_coffe_url(@register_coffe), params: { register_coffe: { coffe_id: @register_coffe.coffe_id, date: @register_coffe.date, employee_id: @register_coffe.employee_id } }
    assert_redirected_to register_coffe_url(@register_coffe)
  end

  test "should destroy register_coffe" do
    assert_difference('RegisterCoffe.count', -1) do
      delete register_coffe_url(@register_coffe)
    end

    assert_redirected_to register_coffes_url
  end
end
