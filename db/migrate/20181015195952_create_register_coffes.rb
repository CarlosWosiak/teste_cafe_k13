class CreateRegisterCoffes < ActiveRecord::Migration[5.2]
  def change
    create_table :register_coffes do |t|
      t.references :coffe, foreign_key: true
      t.references :employee, foreign_key: true
      t.datetime :date

      t.timestamps
    end
  end
end
