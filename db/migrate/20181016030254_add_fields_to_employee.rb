class AddFieldsToEmployee < ActiveRecord::Migration[5.2]
  def change
    add_column :employees, :name, :string
    add_column :employees, :position, :string
    add_column :employees, :special, :boolean    
  end
end
