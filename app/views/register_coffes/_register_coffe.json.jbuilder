json.extract! register_coffe, :id, :coffe_id, :employee_id, :date, :created_at, :updated_at
json.url register_coffe_url(register_coffe, format: :json)
