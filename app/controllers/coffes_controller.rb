class CoffesController < ApplicationController
  before_action :set_coffe, only: [:show, :edit, :update, :destroy]

  # GET /coffes
  # GET /coffes.json
  def index
    @coffes = Coffe.all
  end

  # GET /coffes/1
  # GET /coffes/1.json
  def show
  end

  # GET /coffes/new
  def new
    @coffe = Coffe.new
  end

  # GET /coffes/1/edit
  def edit
  end

  # POST /coffes
  # POST /coffes.json
  def create
    @coffe = Coffe.new(coffe_params)

    respond_to do |format|
      if @coffe.save
        format.html { redirect_to @coffe, notice: 'Coffe was successfully created.' }
        format.json { render :show, status: :created, location: @coffe }
      else
        format.html { render :new }
        format.json { render json: @coffe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /coffes/1
  # PATCH/PUT /coffes/1.json
  def update
    respond_to do |format|
      if @coffe.update(coffe_params)
        format.html { redirect_to @coffe, notice: 'Coffe was successfully updated.' }
        format.json { render :show, status: :ok, location: @coffe }
      else
        format.html { render :edit }
        format.json { render json: @coffe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coffes/1
  # DELETE /coffes/1.json
  def destroy
    @coffe.destroy
    respond_to do |format|
      format.html { redirect_to coffes_url, notice: 'Coffe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coffe
      @coffe = Coffe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def coffe_params
      params.require(:coffe).permit(:name, :special)
    end
end
