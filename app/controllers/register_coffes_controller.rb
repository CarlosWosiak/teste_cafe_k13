class RegisterCoffesController < ApplicationController
  before_action :authenticate_employee!
  before_action :set_register_coffe, only: [:show, :edit, :update, :destroy]


  # GET /register_coffes
  # GET /register_coffes.json
  def index
    @register_coffes = RegisterCoffe.all
  end

  # GET /register_coffes/1
  # GET /register_coffes/1.json
  def show
  end

  # GET /register_coffes/new
  def new
    @register_coffe = current_employee.register_coffes.build
    @coffe = take_coffe_for_user
  end

  # GET /register_coffes/1/edit
  def edit
  end

  # POST /register_coffes
  # POST /register_coffes.json
  def create
    @register_coffe = current_employee.register_coffes.build(register_coffe_params)

    respond_to do |format|
      if @register_coffe.save
        format.html { redirect_to register_coffes_path, notice: 'Tudo certo, aproveita o café ai champs!' }
        format.json { render :index, status: :created, location: @register_coffe }
      else
        format.html { render :new }
        format.json { render json: @register_coffe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /register_coffes/1
  # PATCH/PUT /register_coffes/1.json
  def update
    respond_to do |format|
      if @register_coffe.update(register_coffe_params)
        format.html { redirect_to register_coffes_path, notice: 'Tudo certo, aproveita o café ai champs.' }
        format.json { render :index, status: :ok, location: @register_coffe }
      else
        format.html { render :edit }
        format.json { render json: @register_coffe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /register_coffes/1
  # DELETE /register_coffes/1.json
  def destroy
    @register_coffe.destroy
    respond_to do |format|
      format.html { redirect_to register_coffes_url, notice: 'Tudo certo, aproveita o café ai champs.' }
      format.json { head :no_content }
    end
  end

  private
    def verify_if_user_is_special
      current_employee.special?
    end

    def take_coffe_for_user
      if verify_if_user_is_special
        Coffe.all()
      else
        Coffe.where(special: false)
      end
    end

    
    # Use callbacks to share common setup or constraints between actions.
    def set_register_coffe
      @register_coffe = RegisterCoffe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def register_coffe_params
      params.require(:register_coffe).permit(:coffe_id, :employee_id, :date)
    end
end
